<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Stripe</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Mis estilos css  -->
        <link rel="stylesheet" href="myStyle.css">
    </head>
    <body>
        <header><!--La cabecera del body-->
            <!-- <nav class="navbar navbar-light navbar-expand-lg bg-dark">
                <a href="#" class="navbar-brand text-light">Stripe</a>
                <form class=" navbar-nav my-lg-0 ml-auto">
                    <input type="search" class="form-control p-sm-2 mx-2" placeholder="Search">
                    <input type="submit" class="btn btn-success btn-sm-2" value="Buscar">
                </form>
            </nav> -->
        </header>
        <main class="container"><!--El cuerpo principal del body-->
            <form action="CreateCharge.php" method="POST" id="payment-form" class="mt-5">
                <div class="form-row">
                    <label for="card-element">Tarjeta de credito</label>

                    <div id="card-element">
                    <!--A Stripe element will be inserted here-->
                    </div>

                    <div id="card-errors" role="alert">
                    <!--Used to display form errors-->
                    </div>
                </div>
                <button type="submit" class="btn btn-primary font-italic">Enviar pago</button>
            </form>
        </main>
        <!-- Incluyo es stripe.js  -->
        <script src="https://js.stripe.com/v3/"></script>
        <script src="myStyleDinamic.js"></script>
    </body>
</html>