<?php
	$fields = array("primary_account_number"  => "4545454544444444",
        "expiration_date" => "2403",
        "card_security_code" => "100",
        "card_holder" => array(
            "first_name"  => "John",
            "last_name"  => "Doe",
            "front_name" => "John Doe",
            "telephone"  => "01154660363",
            "email" => "john0doe@mail.com",
            "birth_date" => "01021999"
		),	
        "address"  => array(
                "country" => "Argentina",
                "state" => "Buenos Aires",
                "city" => "Olivos",
                "street" => "San Martin",
                "door_number" => "455"
        ),
        "identification" => array(
                "document_type" => "DNI",
                "document_number" => "12345678"
        )
   );   
   

	$fields_string = http_build_query($fields);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.pagouno.com/v1/Transaction/token");
	curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Content-Type' =>'application/json','Authorization'  => '36f47aa5-a6bc-4ffe-8e50-d3d64822cb3d')); 
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string );
    $data = curl_exec($ch);
    curl_close($ch);
?>