<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PAGOUNO</title>
    <!-- Boostrap css  -->
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css">
    <!-- JQuery, libreria de javascript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <script type="text/javascript" src="wc-pagouno-payments/js/pagouno.js"></script>
    <!-- Mis estilos css  -->
    <style type="text/css">
        *{
            margin: 0;
            padding: 0;
            max-width:100%;
        }
        body{
            background: #F7F3F2;
            font-family: vagrant;
        }
        header div img{
            width: 600px;
            height: 400px;
            margin-bottom: -170px;
            margin-top: -120px;
        }
        main{
            width: 100%;
        }
        main h3{
            font-size: 1.5em;
        }
        main .card-header{
            border: 1px black solid;
        }
        main .card-body{
            border: 1px black solid;
            border-top:none;
            border-radius: 0 0 5px 5px;
        }
        main .alineacion::placeholder{
            text-align: left;
        }
    </style>
</head>
<body class="container">
   <header class="row justify-content-center"><!--Cabecera del contenido-->
        <div class="mx-auto d-block">
            <img src="img/PagoUno.png" alt="Imagen de photoshop">
        </div>
   </header>
   <main class="row justify-content-center">
        <div class="col align-self-center">
            <div class="card-header col-md-12">
                <h3 class="text-center">Ingresar datos</h3>
            </div>
            <div class="card-body bg-light">
                <form action="" method="POST" role="form" id="payment-form">
                    <div class="row">
                        <div class="form-group col-md-5"><!--Numero de la tarjeta-->
                            <label for="pagoUno_ccNo" class="form-label">Número de tarjeta</label>
                            <input type="text" id="pagoUno_ccNo" class="form-control" placeholder="Ingresar numero de tarjeta" maxlength="19" required>
                            <small class="text-sm disable font-italic">Solo incluir números</small>
                        </div>
                        <div class="form-group col-md-2"><!--La fecha de vencimiento-->
                            <label for="dateExpire" class="form-label">Fecha de vencimiento</label>
                            <div class="form-group "><!--El mes-->
                                <input style="width: 60px; display: inline-block;" id="pagoUno_expdate1" placeholder="MM" class="form-control text-right alineacion" required maxlength="2">
                                <!-- <small class="text-sm disable font-italic d-inlina-block">[1-12]</small> -->
                                <span>/</span><!--Para el año-->
                                <input style="width: 60px; display: inline-block;" id="pagoUno_expdate2" placeholder="AA" class="form-control text-right alineacion" required maxlength="2">
                            </div>
                        </div>
                        <div class="form-group col-md-1">
                            <label for="pagoUno_cvc" class="form-label">CVC</label>
                            <input style="width:60px; display: inline-block;" id="pagoUno_cvc" placeholder="CVC" class="form-control text-right alineacion" required maxlength="3">
                            <small class="text-sm font-italic disable">Código</small>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="pagoUno_TypeCard" class="form-label">Tipo de tarjeta</label>
                            <!-- <input type="text" id="pagoUno_cvc" class="form-control text-right alineacion" maxlength="3" placeholder="Código de seguridad" required> -->
                            <select id="pagoUno_TypeCard" class="form-control">
                                <option value="option" disabled selected>Seleccionar tarjeta</option>
                                <option value="Visa">Visa</option>
                                <option value="Mastercar">Mastercar</option>
                                <option value="American Express">American Express</option>
                                <option value="Naranja">Naranja</option>
                                <option value="Cabal">Cabal</option>
                            </select>
                            <small class="text-sm disable font-italic">Seleccione el tipo de tarjeta</small>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4"><!--Nombre del titular-->
                            <label for="pagoUno_ccName" class="form-label">Nombre del titular</label>
                            <input type="text" id="pagoUno_ccName" class="form-control" maxlength="35" placeholder="Ingresar nombre y apellido completo" required>
                            <small class="text-sm disable font-italic">No incluir carácteres especiales o números</small>
                        </div>
                        <div class="form-group col-md-4"><!--Especifica tu número de documento-->
                            <label for="pagoUno_ccDocNum" class="form-label">Número de documento</label>
                            <input type="text" id="pagoUno_ccDocNum" class="form-control" placeholder="Ingresar su documento" maxlength="10">
                            <small class="text-sm font-italic disable">Solo el número de documento</small>
                        </div>
                        <div class="form-group col-md-4"><!--Especifica tu número de documento-->
                            <label for="billing_email" class="form-label">Correo electrónico</label>
                            <input type="text" id="billing_email" class="form-control" placeholder="example@gmail.com">
                            <small class="text-sm font-italic disable">Solo el correo example@gmail.com</small>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-success font-italic">Enviar</button>
                </form>
            </div>
        </div>
   </main>
   <footer>
       <hr class="hr">
   </footer>
   <!-- Este script es mas para las validaciones del formulario  -->
   <script type="text/javascript" src="js/myEfects.js"></script>
</body>
</html>

