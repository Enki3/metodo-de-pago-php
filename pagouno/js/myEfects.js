// Este evento está hecho para que solo se acepte correctamente los datos de la tarjeta
pagoUno_ccNo.addEventListener('keyup', (e) =>{
    let valorInput = e.target.value;
    pagoUno_ccNo.value = valorInput
    //Elimino los espacios en blanco
    .replace(/\s/g, '-')
    //Elimino las letras
    .replace(/\D/g, '')
    //Para poner espacio o simbolo cada cuatro números
    .replace(/([0-9]{4})/g, '$1 ')//Le digo que cada cuatro carácteres me lo agrupe
    //Permite que el último espaciado lo quite
    .trim();
});
// Este evento es para la fecha de vencimiento
pagoUno_expdate1.addEventListener('keyup', (e) =>{
    let valorExpdate = e.target.value;
    pagoUno_expdate1.value = valorExpdate.replace(/\D/g, '');
});
pagoUno_expdate2.addEventListener('keyup', (e) =>{
    let valorExpdate = e.target.value;
    pagoUno_expdate2.value = valorExpdate.replace(/\D/g, '');
});
// Este evento es para el código de la tarjeta
pagoUno_cvc.addEventListener('keyup', (e) =>{
    let valorCvc = e.target.value;
    pagoUno_cvc.value = valorCvc.replace(/\D/g, '');
});
$(document).ready(function(){//Toda JQuery
    // Este evento es para el nombre del titular
    $('#pagoUno_ccName').keypress(function(key){
        window.console.log(key.charCode);
        if((key.charCode <  97 || key.charCode > 122) && (key.charCode < 65 || key.charCode > 90) &&
        (key.charCode != 45) && (key.charCode != 32) && (key.charCode != 225) && (key.charCode != 233) 
        && (key.charCode != 237) && (key.charCode != 243) && (key.charCode != 250) && (key.charCode != 241)
        && (key.charCode != 209))
        return false;
    });
    // Este evento es para el nro de documento
    $('#pagoUno_ccDocNum').keypress(function(key){
        // window.console.log(key.charCode);
        if((key.charCode != 48) && (key.charCode != 49) && (key.charCode != 50) && (key.charCode != 51) &&
        (key.charCode != 52) && (key.charCode != 53) && (key.charCode != 54) && (key.charCode != 55) &&
        (key.charCode != 56) && (key.charCode != 57))
        return false;
    });
});